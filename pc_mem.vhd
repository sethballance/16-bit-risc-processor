----------------------------------------------------------------------------------
-- Company: 		WWU School of Engineering
-- Engineer: 		Seth Ballance
-- 
-- Create Date:   16:43:19 12/09/2019 
-- Design Name: 
-- Module Name:   pc_mem - Behavioral 
-- Project Name: 	16-bit RISC Processor
-- Description: 	Selects which instruction memory address to go to next.
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity pc_mem is
    Port ( pc_plus_one : in  STD_LOGIC_VECTOR (7 downto 0);
           next_address : in  STD_LOGIC_VECTOR (7 downto 0);
           s : in  STD_LOGIC;
           clock : in  STD_LOGIC;
           pc_address : out  STD_LOGIC_VECTOR (7 downto 0) := "00000000");
end pc_mem;

architecture Behavioral of pc_mem is
begin
	process(clock, s)
	begin
		if rising_edge(clock) then
			if s = '0' then
				pc_address <= pc_plus_one;
			else
				pc_address <= next_address;
			end if;
		end if;
	end process;
end Behavioral;

