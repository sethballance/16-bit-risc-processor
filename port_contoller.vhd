----------------------------------------------------------------------------------
-- Company: 		WWU School of Engineering
-- Engineer: 		Seth Ballance
-- 
-- Create Date:   16:28:33 12/10/2019
-- Design Name: 
-- Module Name:   port_contoller - Behavioral 
-- Project Name: 	16-bit RISC Processor
-- Description: 	Fetches and stores data on ports.
-- Additional Comments: Uses the switches as input vector and LEDs as output vector
--	
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity port_contoller is
    Port ( write_enable : in  STD_LOGIC;
			  switch : in  STD_LOGIC_VECTOR (7 downto 0);
			  LED : out  STD_LOGIC_VECTOR (7 downto 0);
           write_data : in  STD_LOGIC_VECTOR (7 downto 0);
			  clock : in  STD_LOGIC;
           data_out : out  STD_LOGIC_VECTOR (7 downto 0));
end port_contoller;

architecture Behavioral of port_contoller is
begin
	process(clock, write_enable)
	begin
		if rising_edge(clock) then --read
			data_out <= switch;
		end if;
		
		if (falling_edge(clock) and (write_enable = '1')) then --write
			LED <= write_data;
		end if;
	end process;
end Behavioral;

