----------------------------------------------------------------------------------
-- Company: 		WWU School of Engineering
-- Engineer: 		Seth Ballance
-- 
-- Create Date:   18:30:51 12/09/2019 
-- Design Name: 
-- Module Name:   register_mem - Behavioral 
-- Project Name: 	16-bit RISC Processor
-- Description: 	Fetches and stores register data
-- Additional Comments: Outputs data on rising edge of clock and inputs data
-- on falling edge.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity register_mem is
    Port ( read_reg1 : in  STD_LOGIC_VECTOR (2 downto 0);
           read_reg2 : in  STD_LOGIC_VECTOR (2 downto 0);
           write_data : in  STD_LOGIC_VECTOR (7 downto 0);
           data1 : out  STD_LOGIC_VECTOR (7 downto 0);
           data2 : out  STD_LOGIC_VECTOR (7 downto 0);
           write_enable : in  STD_LOGIC; -- from controller
			  clock : in  STD_LOGIC);
end register_mem;

architecture Behavioral of register_mem is
	signal R0	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R1	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R2	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R3	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R4	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R5	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R6	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
	signal R7	:	STD_LOGIC_VECTOR (7 downto 0) := x"00";
begin
	process(clock, write_enable, read_reg1, read_reg2)
	begin
		if (falling_edge(clock) and (write_enable = '1')) then
			case read_reg1 is
				when "000" =>
					R0 <= write_data;
				when "001" =>
					R1 <= write_data;
				when "010" =>
					R2 <= write_data;
				when "011" =>
					R3 <= write_data;
				when "100" =>
					R4 <= write_data;
				when "101" =>
					R5 <= write_data;
				when "110" =>
					R6 <= write_data;
				when "111" =>
					R7 <= write_data;
				when others =>
			end case;
		end if;
		
		if rising_edge(clock) then
			case read_reg1 is
				when "000" =>
					data1 <= R0;
				when "001" =>
					data1 <= R1;
				when "010" =>
					data1 <= R2;
				when "011" =>
					data1 <= R3;
				when "100" =>
					data1 <= R4;
				when "101" =>
					data1 <= R5;
				when "110" =>
					data1 <= R6;
				when "111" =>
					data1 <= R7;
				when others =>
			end case;
			
			case read_reg2 is
				when "000" =>
					data2 <= R0;
				when "001" =>
					data2 <= R1;
				when "010" =>
					data2 <= R2;
				when "011" =>
					data2 <= R3;
				when "100" =>
					data2 <= R4;
				when "101" =>
					data2 <= R5;
				when "110" =>
					data2 <= R6;
				when "111" =>
					data2 <= R7;
				when others =>
			end case;
		end if;
	end process;
end Behavioral;

