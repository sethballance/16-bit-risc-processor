----------------------------------------------------------------------------------
-- Company: 		WWU School of Engineering
-- Engineer: 		Seth Ballance
-- 
-- Create Date:   23:11:52 12/08/2019 
-- Design Name: 
-- Module Name:   pc_adder - Behavioral 
-- Project Name: 	16-bit RISC Processor
-- Description: 	Adds 1 to the pc for the next instruction
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity pc_adder is
    Port (	pc : in  STD_LOGIC_VECTOR (7 downto 0);
				pc_plus_1 : out  STD_LOGIC_VECTOR (7 downto 0));
end pc_adder;

architecture Behavioral of pc_adder is
begin
	pc_plus_1 <= pc + '1';
end Behavioral;

