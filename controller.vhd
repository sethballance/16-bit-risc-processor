----------------------------------------------------------------------------------
-- Company: 		WWU School of Engineering
-- Engineer: 		Seth Ballance
-- 
-- Create Date:   17:10:17 12/05/2019 
-- Design Name: 
-- Module Name:   controller - Behavioral 
-- Project Name: 	16-bit RISC Processor
-- Description: 	Sends out signal to parts to let them know what function to run
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity controller is
    Port ( opcode : in  STD_LOGIC_VECTOR (4 downto 0);
           pc_mux : out  STD_LOGIC;
           port_mem : out  STD_LOGIC;
           alu_mux : out  STD_LOGIC_VECTOR (1 downto 0);
           reg_mem : out  STD_LOGIC);
end controller;

architecture Behavioral of controller is
begin
	process(opcode)
	begin
		pc_mux <= '0';
		port_mem <= '0';
		alu_mux <= "10";
		reg_mem <= '0';
		
		-- if J-type or HALT
		if ((opcode(4 downto 2) = "001") or (opcode(4 downto 0) = "00000")) then
			pc_mux <= '1';
		end if;
		
		-- if PO or PI
		if (opcode(4 downto 0) = "01101") then
			port_mem <= '1';
		end if;
			
		-- if I-type
		if (opcode(4) = '1') then
			alu_mux <= "00";
		-- if PO or PI
		elsif (opcode(4 downto 1) = "0110") then
			alu_mux <= "01";
		end if;
		
		-- if I-type or R-type
		if ((opcode(4) = '1') or (opcode(4 downto 3) = "01")) then
			reg_mem<= '1';
		end if;
		
	end process;
end Behavioral;

